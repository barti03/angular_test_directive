describe('directive: myInput', function(){
  var element, scope;

  beforeEach(function(){
    module('app');
  });

  describe('generally', function(){
    beforeEach(inject(function($rootScope, $compile){
      scope = $rootScope.$new();
      element = '<my-input></my-input>';
      element = $compile(element)(scope);
      scope.$digest();
    }));

    it('renders input', function(){
      expect(element[0].tagName).toBe('INPUT');
    });
  });

  describe('when attribute color is specified and equal to red', function(){
    beforeEach(inject(function($rootScope, $compile){
      scope = $rootScope.$new();
      element = '<my-input color="red"></my-input>';
      element = $compile(element)(scope);
      scope.$digest();
    }));

    it('renders element with red background', function(){
      expect(element.attr('style')).toBe('background-color: red');
    });
  });

  describe('when directive has content "My Message"', function(){
    beforeEach(inject(function($rootScope, $compile){
      scope = $rootScope.$new();
      element = '<my-input>My Message</my-input>';
      element = $compile(element)(scope);
      scope.$digest();
    }));

    it('renders element with placeholder "My Message"', function(){
      expect(element.attr('placeholder')).toBe('My Message');
    });
  });
});
