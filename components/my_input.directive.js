angular.module('app').directive('myInput', function(){
  return {
    restrict: 'E',
    replace: true,
    template: '<input style="background-color: {{color}}" placeholder="{{content}}"/>',
    transclude: true,
    scope: {
      color: "@"
    },
    compile: function(elements, attrs, transclude){
      return {
        pre: function(scope){
          transclude(scope, function(clone){
            if(clone && clone[0]){
              scope.content = clone[0].textContent;
            }
          });
        }
      };
    }
  };
});
